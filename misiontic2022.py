import tkinter as tk 
from tkinter import *
from PIL import Image, ImageTk

from tkinter import messagebox

#Make Interface

windows=tk.Tk()
windows.title('"Resto MisionTic2022 Ruta 2')
windows.geometry('450x450')

#Make Command
def imagen():
    img= Image.open('logo-mision.png')
    new_image= img.resize((400,200))
    render=ImageTk.PhotoImage(new_image)
    img1= Label(windows, image=render)

    img1.image= render
    img1.place(x=10, y=30)

def message():
    messagebox.showinfo("Message", "Hola Mision TIC 2022... Soy Rafael Chica Pretelt")

#Make Button
viewimage= tk.Button(windows,command=imagen, text='view image', height=2, width=15)
viewimage.place(x=140, y=250)

viewmessage= tk.Button(windows,command= message, text='view message', height=2, width=15)
viewmessage.place(x=140, y=350)


windows.mainloop()